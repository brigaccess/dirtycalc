unit calc;

{$mode objfpc}{$H+}{$Q+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, LCLType, math;

type

  TStates = (Ready, Sign, Armed, Result, Error);
  TOps = (None, Add, Subtract, Multiply, Divide);
  { TCalcForm }

  TCalcForm = class(TForm)
    OpButtonClearExpression: TButton;
    OpButtonClear: TButton;
    OpButtonBackspace: TButton;
    OpButtonAdd: TButton;
    OpButtonSubtract: TButton;
    OpButtonMultiply: TButton;
    OpButtonDivide: TButton;
    OpButtonSqrt: TButton;
    OpButtonPercent: TButton;
    OpButtonReverse: TButton;
    OpButtonInvert: TButton;
    OpButtonEquals: TButton;
    NumButtonDot: TButton;
    NumButton0: TButton;
    NumButton1: TButton;
    NumButton2: TButton;
    NumButton3: TButton;
    NumButton4: TButton;
    NumButton5: TButton;
    NumButton6: TButton;
    NumButton7: TButton;
    NumButton8: TButton;
    NumButton9: TButton;
    MemButtonPlus: TButton;
    MemButtonMinus: TButton;
    MemButtonSave: TButton;
    MemButtonRead: TButton;
    MemButtonClear: TButton;
    ShapeBackground: TShape;
    StaticTextHistory: TStaticText;
    StaticTextScreen: TStaticText;
    StaticTextMemState: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MemButtonClearClick(Sender: TObject);
    procedure MemButtonMinusClick(Sender: TObject);
    procedure MemButtonPlusClick(Sender: TObject);
    procedure MemButtonReadClick(Sender: TObject);
    procedure MemButtonSaveClick(Sender: TObject);
    procedure NumButtonClick(Sender: TObject);
    procedure OpButtonClick(Sender: TObject);
    procedure OpButtonBackspaceClick(Sender: TObject);
    procedure OpButtonClearClick(Sender: TObject);
    procedure OpButtonClearExpressionClick(Sender: TObject);
    procedure OpButtonEqualsClick(Sender: TObject);
    procedure OpButtonInvertClick(Sender: TObject);
    procedure OpButtonPercentClick(Sender: TObject);
    procedure OpButtonReverseClick(Sender: TObject);
    procedure OpButtonSqrtClick(Sender: TObject);
  private
    procedure AddSymbol(Symbol: Char);
    procedure ArmOp(Op: TOps);
    procedure Backspace();
    procedure Calculate();
    procedure Clear();
    procedure ClearExpression();
    procedure EqualsOp();
    function getSign(Op: TOps): Char;
    function isZero(): boolean;
    procedure UpdateDisplay();
  public
    { public declarations }
  end;

var
  CalcForm: TCalcForm;
  Dot: Char;

  State: TStates = Ready;
  Operation: TOps = None;
  Expression: String = '0';
  Buffer: Extended = 0;

  LatestArg: Extended = 0;

  Memory: Extended = 0;

  History: String = '';

implementation

{$R *.lfm}

{ TCalcForm }

procedure TCalcForm.NumButtonClick(Sender: TObject);
var
  SenderButton: TButton;
begin
  SenderButton := Sender as TButton;
  AddSymbol(SenderButton.Caption[1]);
end;

procedure TCalcForm.OpButtonClick(Sender: TObject);
var
  SButton: TButton;
begin
  SButton := (Sender as TButton);
  case SButton.Caption of
    '+': ArmOp(Add);
    '-': ArmOp(Subtract);
    '×': ArmOp(Multiply);
    '÷': ArmOp(Divide);
  end;
end;

procedure TCalcForm.OpButtonBackspaceClick(Sender: TObject);
begin
  Backspace();
end;

procedure TCalcForm.OpButtonClearClick(Sender: TObject);
begin
  Clear();
end;

procedure TCalcForm.OpButtonClearExpressionClick(Sender: TObject);
begin
  ClearExpression();
end;

procedure TCalcForm.OpButtonEqualsClick(Sender: TObject);
begin
  EqualsOp();
end;

procedure TCalcForm.OpButtonInvertClick(Sender: TObject);
begin
  // invert(Expression)
  if State <> Error then begin
    if (Length(Expression) > 0) and (Expression <> '0') then begin
      if(Expression[1] = '-') then
        Expression := Copy(Expression, 2, Length(Expression))
      else
        Expression := '-' + Expression;

      if State = Sign then State := Armed
      else if State = Result then State := Ready;

      UpdateDisplay();
    end;
  end;
end;

procedure TCalcForm.OpButtonPercentClick(Sender: TObject);
begin
  // Nothing
  if State <> Error then begin
    Expression := FloatToStr(Buffer * (StrToFloat(Expression) / 100));

    if State = Sign then begin
      State := Armed;
    end
    else if State = Result then State := Ready;

    UpdateDisplay();
  end;
end;

procedure TCalcForm.OpButtonReverseClick(Sender: TObject);
var
  Num: extended;
begin
  // reciproc(Expression)
  if State <> Error then begin
    Num := StrToFloat(Expression);

    if Num <> 0 then begin
      Expression := FloatToStr(1 / Num);
    end
    else begin
      State := Error;
      Expression := 'Деление на ноль невозможно';
    end;

    if State = Sign then State := Armed
    else if State = Result then State := Ready;

    UpdateDisplay();
  end;
end;

procedure TCalcForm.OpButtonSqrtClick(Sender: TObject);
var
  Num: extended;
begin
  // sqrt(Expression)
  if State <> Error then begin
    Num := StrToFloat(Expression);

    if Num < 0 then begin
      State := Error;
      Expression := 'Недопустимый ввод';
      UpdateDisplay();
      exit;
    end
    else
      Expression := FloatToStr(sqrt(Num));

    if State = Sign then State := Armed
    else if State = Result then State := Ready;

    UpdateDisplay();
  end;
end;

procedure TCalcForm.FormCreate(Sender: TObject);
begin
  Dot := DefaultFormatSettings.DecimalSeparator;
  NumButtonDot.Caption := Dot;
  UpdateDisplay();
end;

procedure TCalcForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if InRange(Key, VK_0, VK_9) then begin
       AddSymbol(chr(48 + Key - VK_0));
       exit;
    end;

    if InRange(Key, VK_NUMPAD0, VK_NUMPAD9) then begin
       AddSymbol(chr(48 + Key - VK_NUMPAD0));
       exit;
    end;

    case Key of
      VK_BACK: Backspace();
      VK_DELETE: ClearExpression();
      VK_RETURN: begin
        EqualsOp();
        Key := VK_UNKNOWN;
      end;
      VK_ESCAPE: Clear();

      VK_DECIMAL: AddSymbol(Dot);
      VK_OEM_COMMA: AddSymbol(Dot);
      VK_ADD: ArmOp(Add);
      VK_SUBTRACT: ArmOp(Subtract);
      VK_OEM_PLUS: ArmOp(Add);
      VK_OEM_MINUS: ArmOp(Subtract);
      VK_MULTIPLY: ArmOp(Multiply);
      VK_DIVIDE: ArmOp(Divide);

      VK_R: OpButtonReverse.Click;
      VK_F9: OpButtonInvert.Click;
    end;
end;

procedure TCalcForm.MemButtonClearClick(Sender: TObject);
begin
     if State <> Error then begin
       Memory := 0;
       UpdateDisplay();
     end;
end;

procedure TCalcForm.MemButtonMinusClick(Sender: TObject);
begin
  if State <> Error then begin
    Memory -= StrToFloat(Expression);
    UpdateDisplay();
  end;
end;

procedure TCalcForm.MemButtonPlusClick(Sender: TObject);
begin
  if State <> Error then begin
    Memory += StrToFloat(Expression);
    UpdateDisplay();
  end;
end;

procedure TCalcForm.MemButtonReadClick(Sender: TObject);
begin
    if State <> Error then begin
       Expression := FloatToStr(Memory);

       if State = Sign then State := Armed
       else if State = Result then State := Ready;

       UpdateDisplay();
    end;
end;

procedure TCalcForm.MemButtonSaveClick(Sender: TObject);
begin
  if State <> Error then begin
     Memory := StrToFloat(Expression);
     UpdateDisplay();
  end;
end;

procedure TCalcForm.AddSymbol(Symbol: Char);
begin
  if (State <> Error) then begin
     if (Symbol = Dot) and (Pos(Dot, Expression) <> 0) then
        exit;

     if (State = Sign) then begin
        Expression := '0';
        State := Armed;
     end;

     if (State = Result) then begin
        Expression := '0';
        State := Ready;
     end;

     if (Length(Expression) > 15) and (Symbol <> Dot) then
        exit;

     if isZero then
        Expression := Symbol
     else
        Expression += Symbol;

     UpdateDisplay();
  end;
end;

procedure TCalcForm.ArmOp(Op: TOps);
begin
     if State <> Error then begin
        Operation := Op;
        case State of
          Ready: begin
            Buffer := StrToFloat(Expression);
            History := Expression;
          end;
          Result: begin
            Buffer := StrToFloat(Expression);
            History := Expression;
          end;
          Armed: begin
            Calculate();
          end;
          Sign: begin
            if Length(History) > 1 then
              History[Length(History)] := getSign(Operation);
          end;
        end;

        if State <> Sign then
          History += ' ' + getSign(Operation);

        State := Sign;
        UpdateDisplay();
     end;
end;

procedure TCalcForm.Backspace;
begin
  if (State <> Error) and (State <> Result) and (Length(Expression) > 0)
  then begin
    Expression := Copy(Expression, 1, Length(Expression) - 1);

    if (Expression = '') or (Expression = '-') then Expression := '0';
    UpdateDisplay();
  end;
end;

procedure TCalcForm.Calculate;
begin
  if State <> Error then begin
    if State = Armed then
        LatestArg := StrToFloat(Expression);

    if (State = Ready) and (Operation <> None) then
        Buffer := StrToFloat(Expression);

    case Operation of
      Add: Buffer += LatestArg;
      Subtract: Buffer -= LatestArg;
      Multiply: Buffer *= LatestArg;
      Divide: begin
        if LatestArg <> 0 then begin
          Buffer /= LatestArg;
        end
        else begin
          Expression := 'Деление на ноль невозможно';
          State := Error;
          UpdateDisplay();
          exit;
        end;
      end;
    end;

    Expression := FloatToStr(Buffer);
    UpdateDisplay();
  end;
end;

procedure TCalcForm.Clear;
begin
  State := Ready;
  Operation := None;
  Expression := '0';
  Buffer := 0;
  LatestArg := 0;
  History := '';
end;

procedure TCalcForm.ClearExpression;
begin
  if (State = Error) then begin
    Clear();
    exit;
  end;
  if (State = Result) then begin
    Buffer := 0;
  end;
  Expression := '0';
  UpdateDisplay();
end;

procedure TCalcForm.EqualsOp;
begin
  if Operation <> None then begin
     Calculate();
     if (State <> Error) then State := Result;
     History := '';
     UpdateDisplay();
  end;
end;

function TCalcForm.getSign(Op: TOps): Char;
begin
  case Op of
    Add: getSign := '+';
    Subtract: getSign := '-';
    Multiply: getSign := '*';
    Divide: getSign := '/';
    else
        getSign := ' ';
  end;
end;

function TCalcForm.isZero: boolean;
begin
  isZero := (Length(Expression) = 0) or (Expression[1] = '0')
end;

procedure TCalcForm.UpdateDisplay;
begin
     StaticTextScreen.Caption := Expression;
     if Length(Expression) > 23 then begin
        StaticTextScreen.Font.Height := 14;
        StaticTextScreen.Top := 50;
     end
     else if Length(Expression) > 11 then begin
        StaticTextScreen.Font.Height := 20;
        StaticTextScreen.Top := 46;
     end
     else begin
        StaticTextScreen.Font.Height := 24;
        StaticTextScreen.Top := 43;
     end;

     StaticTextMemState.Visible := Memory <> 0;

     if (State = Error) then
       History := '';

     StaticTextHistory.Caption := History;

end;


end.

